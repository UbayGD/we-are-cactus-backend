import { Body, Controller, Get, Post, Req, Res, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { JwtAuthGuard } from './auth/jwt-auth.guard';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('login')
  login(@Body() body: any) {
    const {email, password} = body;
    return this.appService.login(email, password);
  }

  @UseGuards(JwtAuthGuard)
  @Get('products')
  getProducts(@Req() req) {    
    return this.appService.getProducts();
  }
}
